let userName = "";

while ( userName.length === 0 ) {
    userName = prompt("Please enter your name: ", userName);

    if( userName === null ) {
        alert("Goodbye!");
    }
}

let userAge;

while ( isNaN(userAge) || (userAge.length === 0) ) {
    userAge = prompt("Please enter your age: ", userAge);

    if (userAge === null) {
        alert("Goodbye!");

    } else if ( isNaN(userAge) || (userAge.length === 0) ) {
        alert("The age you entered is invalid.");

    } else if ( userAge < 18) {
        alert("You are not allowed to visit this website.");

    } else if ( userAge > 22 ) {
        alert("Welcome, " + userName);

    } else if ( confirm("Are you sure you want to continue?") ) {
        alert("Welcome, " + userName);

    } else {
        alert("You are not allowed to visit this website.");
        break;
    }
}
